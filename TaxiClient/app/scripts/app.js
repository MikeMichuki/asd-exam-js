'use strict';

/**
 * @ngdoc overview
 * @name taxiClientApp
 * @description
 * # taxiClientApp
 *
 * Main module of the application.
 */
angular
  .module('taxiClientApp', [
    'ngResource',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
