'use strict';

angular.module('taxiClientApp')
  .controller('MainCtrl', function ($scope, BookingsSyncService) {
    $scope.name = '';
    $scope.phone = '';
    $scope.pickupAddress = '';
    $scope.syncNotification = '';
    $scope.submit = function() {
      BookingsSyncService.save({
        'customer_name': $scope.name,
        'phone': $scope.phone,
        'pickup_address': $scope.pickupAddress,
      }, function(data) {
        $scope.syncNotification = data;
      });
    };
  });
