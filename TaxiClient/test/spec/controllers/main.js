'use strict';

describe('Controller: MainCtrl', function () {

  beforeEach(module('taxiClientApp'));

  var MainCtrl,
    scope;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should hold booking information', function () {
    expect(scope.name).toBeDefined();
    expect(scope.phone).toBeDefined();
    expect(scope.pickupAddress).toBeDefined();
    expect(scope.submit).toBeDefined();
    expect(scope.syncNotification).toBeDefined();
  });

  var $httpBackend = {},
    BookingsSyncService = {};

  beforeEach(inject(function(_$httpBackend_, _BookingsSyncService_) {
    $httpBackend = _$httpBackend_;
    BookingsSyncService = _BookingsSyncService_;
  }));

  afterEach(function(){
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should submit a request to the backend service', function() {
    $httpBackend
      .expectPOST('http://localhost:3000/bookings',
                  {booking: {customer_name: 'John Smith', phone: '123', pickup_address: 'here'}})
      .respond(201, {bookingId: 1, message: 'Booking request is being processed'});

    // We assume a user entered some information in the HTTP form
    scope.name = 'John Smith';
    scope.phone = '123';
    scope.pickupAddress = 'here';

    // Let's simulate the user clicks 'Submit'
    scope.submit();
    $httpBackend.flush();

    // Now the expectations!
    expect(scope.syncNotification)
      .toEqual('Booking request is being processed');
    expect(BookingsSyncService.getLastBookingId())
      .toEqual(1);
  });

});
