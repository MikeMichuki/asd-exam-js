class Booking < ActiveRecord::Base
  attr_accessible :customer_name, :phone, :pickup_address, :pickup_time, :status

  belongs_to :taxi
end
