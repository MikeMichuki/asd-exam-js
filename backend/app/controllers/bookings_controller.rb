class BookingsController < ApplicationController
  def create
    b = Booking.create(params[:booking])
    TaxiAllocatorJob.new.async.perform(b.id)
    render :json => {:message => "Booking is being processed"}, :status => :created
  end
  def updateTaxiAssignment
    b = params[:booking]
    booking = Booking.find(b[:id])

    render :nothing => true, :status => :ok
  end
end
