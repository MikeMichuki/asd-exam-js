Backend::Application.routes.draw do
  post '/bookings', :to => 'bookings#create'
  post '/taxiAssignments', :to => 'bookings#updateTaxiAssignment'
  match '*all' => 'application#cors_preflight', :constraints => {:method => 'OPTIONS'}
end
