Given(/^I have access to STRS via its web application$/) do
  FactoryGirl.create(:bus_station)
  @customer = Capybara::Session.new(:poltergeist)
  @customer.visit 'http://localhost:9000/#/'
end

Given(/^a Taxi driver is waiting for ride requests$/) do
  @taxi_driver = Capybara::Session.new(:poltergeist)
  @taxi_driver.visit 'http://localhost:9090/#/'
end

Given(/^I enter my current address and contact information$/) do
  @customer.fill_in('Name', with: 'Juan Perez')
  @customer.fill_in('Pickup Address', with: 'Liivi 2')
  @customer.fill_in('Phone', with: '+372 1234 4321')
end

Given(/^I submit a booking request$/) do
  @customer.click_button('Submit')
end

When(/^the taxi driver accepts the request$/) do
  accept_button = @taxi_driver.find("#accept")
  @taxi_driver.save_screenshot('screenshot.png')
  accept_button.click()
end

Then(/^I should be notified about the fact that my request is being processed$/) do
  expect(@customer.find_by_id('sync_notification').text).to eq('Booking is being processed')
end

Then(/^I should eventually receive a booking confirmation with its corresponding details$/) do
  pending # express the regexp above with the code you wish you had
end
