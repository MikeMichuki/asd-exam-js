Feature: Taxi booking with driver confirmation
  As a customer
  So that I can have a taxi picking me up at a given address
  I want to book the taxi via STRS

  @javascript
  Scenario: Taxi driver confirms
    Given I have access to STRS via its web application
      And a Taxi driver is waiting for ride requests
      And I enter my current address and contact information
      And I submit a booking request
     When the taxi driver accepts the request
     Then I should be notified about the fact that my request is being processed
      And I should eventually receive a booking confirmation with its corresponding details
