'use strict';

var app = angular.module('taxiHomeApp');

app.controller('AsyncNotifModalController', function ($scope, STRSService) {
  var booking = STRSService.getBooking();
  $scope.name = booking.name;
  $scope.address = booking.address;
  $scope.phone = booking.phone;
  $scope.accept = function () { STRSService.notifyDecision('ACCEPTED'); };
  $scope.reject = function () { STRSService.notifyDecision('REJECTED'); };
});

app.factory('AsyncNotifModal', function (btfModal) {
  return btfModal({
    controller: 'AsyncNotifModalController',
    controllerAs: 'modal',
    templateUrl: 'views/modal.html'
  });
});
