'use strict';

/**
 * @ngdoc overview
 * @name taxiHomeApp
 * @description
 * # taxiHomeApp
 *
 * Main module of the application.
 */
angular
  .module('taxiHomeApp', [
    'ngRoute',
    'btford.modal'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
